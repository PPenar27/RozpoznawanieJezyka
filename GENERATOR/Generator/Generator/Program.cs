﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BIAI
{
    class Generator
    {
        static string inputfile = @"input.txt";
        static string outputfile = @"output.txt";

        static char[] chars = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', // latin
        'ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ź', 'ż', // polish
        'ä', 'ö', 'ß', 'ü', // german
        'à', 'â', 'ç', 'é', 'è', 'ê', 'ë', 'î', 'ï', 'ô', 'û', 'ù', 'ÿ', // french
        'ñ', // spanish
        'ì', 'ò', // italian
        'á', 'č', 'ě', 'í', 'ň', 'ř', 'š', 'ú', 'ý', 'ž', // czech
        'ɑ', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', // russian
        'ğ', 'ş' // turkish
        };

        public static void GenerateLetterFrequencies()
        {
            int total = 0;
            var freq = new Dictionary<char, double>();
            /** init map of frequencies **/
            foreach (char chr in chars)
            {
                freq.Add(chr, 0.00);
            }

            /** read letters from input file **/
            using (StreamReader r = new StreamReader(inputfile, Encoding.UTF8))
            {
                char[] buffer = new char[1024];
                int read;
                while ((read = r.ReadBlock(buffer, 0, buffer.Length)) > 0)
                {
                    for (int i = 0; i < read; i++)
                    {
                        if (Char.IsLetter(buffer[i]))
                        {
                            char c = Char.ToLower(buffer[i]);
                            if (freq.ContainsKey(c))
                            {
                                freq[c]++;
                                total++;
                            }
                        }
                    }
                }
            }

            /** write frequencies into output file **/
            using (StreamWriter writer = new StreamWriter(outputfile, false, Encoding.UTF8))
            {
                foreach (char key in freq.Keys.ToList())
                {
                    freq[key] = freq[key] / (double)total; ;
                    writer.Write(freq[key].ToString("F8").Replace(",", ".") + " ");
                }
            }
        }

        public static void Main(string[] args)
        {

            while (true)
            {
                Console.WriteLine("Menu:");
                Console.WriteLine("0 - generate");
                Console.WriteLine("1 - exit");

                string result = Console.ReadLine();
                if (result == "0")
                {
                    GenerateLetterFrequencies();
                }
                else if (result == "1")
                {
                    System.Environment.Exit(0);
                }
                Console.WriteLine();
            }
        }

    }
}
