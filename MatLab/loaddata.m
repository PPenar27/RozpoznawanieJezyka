% Klasyfikacja jezykow
% zaladowanie danych treningowych
% osobno dla wejsc
% osobno dla wyjsc 
inputs   = importdata("in.train");
targets  = importdata("out.train");
testdata = importdata("freq.test");

inputs   = inputs';
targets  = targets';
testdata = testdata';

langs = ["Polski", "Angielski", "Niemiecki", "Francuski", "Hiszpanski", "Wloski", "Rosyjski", "Czeski", "Turecki", "Lacinski"];

% Create a Pattern Recognition Network
hiddenLayerSize = 13;
net = patternnet(hiddenLayerSize);

% Set up Division of Data for Training, Validation, Testing
net.divideParam.trainRatio = 80/100;
net.divideParam.testRatio = 20/100;

%{
 Neural Network Toolbox Training Functions.
 
  To change a neural network's training algorithm set the net.trainFcn
  property to the name of the corresponding function.  For example, to use
  the scaled conjugate gradient backprop training algorithm:

 
    net.trainFcn = 'trainscg';
 
  Backpropagation training functions that use Jacobian derivatives
 
    These algorithms can be faster but require more memory than gradient
    backpropation.  They are also not supported on GPU hardware.
 
    trainlm   - Levenberg-Marquardt backpropagation.
    trainbr   - Bayesian Regulation backpropagation.
 
  Backpropagation training functions that use gradient derivatives
 
    These algorithms may not be as fast as Jacobian backpropagation.
    They are supported on GPU hardware with the Parallel Computing Toolbox.
 
    trainbfg  - BFGS quasi-Newton backpropagation.
    traincgb  - Conjugate gradient backpropagation with Powell-Beale restarts.
    traincgf  - Conjugate gradient backpropagation with Fletcher-Reeves updates.
    traincgp  - Conjugate gradient backpropagation with Polak-Ribiere updates.
    traingd   - Gradient descent backpropagation.
    traingda  - Gradient descent with adaptive lr backpropagation.
    traingdm  - Gradient descent with momentum.
    traingdx  - Gradient descent w/momentum & adaptive lr backpropagation.
    trainoss  - One step secant backpropagation.
    trainrp   - RPROP backpropagation.
    trainscg  - Scaled conjugate gradient backpropagation.
 
  Supervised weight/bias training functions
 
    trainb    - Batch training with weight & bias learning rules.
    trainc    - Cyclical order weight/bias training.
    trainr    - Random order weight/bias training.
    trains    - Sequential order weight/bias training.
 
  Unsupervised weight/bias training functions
 
    trainbu   - Unsupervised batch training with weight & bias learning rules.
    trainru   - Unsupervised random order weight/bias training. 
%}
net.trainFcn = 'traincgp';
% Train the Network
[net,tr] = train(net, inputs, targets);

% Test the Network
outputs = net(testdata);
errors = gsubtract(targets, outputs);
performance = perform(net, targets, outputs);

% View the Network
view(net)

max  = 0.0;
iter = 0;
for i = 1:length(outputs)
    % szukamy najbardziej pasujacego wyniku
    if outputs(i) > max
       max = outputs(i);
       iter = i;
    end
    disp(langs(i) + " " + outputs(i));
end

disp("Prawdopodobny jezyk: " + langs(iter));
