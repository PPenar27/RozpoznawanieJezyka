﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIAI
{
    class Program
    {
        static void Main(string[] args)
        {

            while (true)
            {
                Console.WriteLine("Menu:");
                Console.WriteLine("0 - train");
                Console.WriteLine("1 - test");
                Console.WriteLine("2 - exit");

                string result = Console.ReadLine();
                if (result == "0")
                {
                    Train.trainNetwork();
                }
                else if (result == "1")
                {
                    Test.testNetwork();
                }
                else if (result == "2")
                {
                    System.Environment.Exit(0);
                }
                Console.WriteLine();
            }
        }
    }
}

