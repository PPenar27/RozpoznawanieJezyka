﻿using System;
using FANNCSharp;
using System.IO;
/**
#if FANN_FIXED
using FANNCSharp.Fixed;
using DataType = System.Int32;
#elif FANN_DOUBLE
using FANNCSharp.Double;
using DataType = System.Double;
#else
using FANNCSharp.Float;
using DataType = System.Single;
#endif
    **/
using FANNCSharp.Double;
using DataType = System.Double;

namespace BIAI
{
class Test
    {
        public static void testNetwork()
        {
            //load data form file
            string[] lines = File.ReadAllLines("frequencies.test");
            string[] param = lines[0].Split(null);
            int neurons_count = Int32.Parse(param[0]);
            int output_neurons_count = Int32.Parse(param[1]);
            double[] input = new double[neurons_count];

            string[] ele = lines[1].Split(null);
            for (int i = 0; i < neurons_count; i++)
            {
                input[i] = double.Parse(ele[i], System.Globalization.CultureInfo.InvariantCulture);
            }

            using (NeuralNet net = new NeuralNet("language_classify.net"))
            {
                float max = 0.0f;
                int index = 0, i = 0;
                double[] outputs = net.Run(input);
                foreach(float output in outputs)
                {
                    if (output > max)
                    {
                        max = output;
                        index = i;
                    }
                    Console.WriteLine(GetLanguageByIndex(i) + " :" + Math.Abs(output).ToString());
                    i++;
                }
                Console.WriteLine();
                Console.WriteLine("Prawdopodobny jezyk: " + GetLanguageByIndex(index));
                Console.ReadLine();
            }
        }

        public static string GetLanguageByIndex(int i)
        {
            string ret = "";
            switch (i)
            {
                case 0:
                    ret = "Polski";
                    break;
                case 1:
                    ret = "Angielski";
                    break;
                case 2:
                    ret = "Niemiecki";
                    break;
                case 3:
                    ret = "Francuski";
                    break;
                case 4:
                    ret = "Hiszpański";
                    break;
                case 5:
                    ret = "Włoski";
                    break;
                case 6:
                    ret = "Rosyjski";
                    break;
                case 7:
                    ret = "Czeski";
                    break;
                case 8:
                    ret = "Turecki";
                    break;
                case 9:
                    ret = "Łaciński";
                    break;
            }
            return ret;
        }

    };
}
