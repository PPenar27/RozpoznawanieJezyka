﻿using System;
using System.IO;
using FANNCSharp;
/**
#if FANN_FIXED
using FANNCSharp.Fixed;
using DataType = System.Int32;
#elif FANN_DOUBLE
using FANNCSharp.Double;
using DataType = System.Double;
#else
using FANNCSharp.Float;
using DataType = System.Single;
#endif
    **/
using FANNCSharp.Double;
using DataType = System.Double;

namespace BIAI
{
    class Train
    {
        static string filename = @"frequencies.train";
        public static void trainNetwork()
        {
            Boolean czyNormalizowac = false;

            //load data form file
            string[] lines = File.ReadAllLines(filename);
            string[] param = lines[0].Split(null);
            int input_count = Int32.Parse(param[0]);
            uint input_neurons_count = UInt32.Parse(param[1]);
            uint output_neurons_count = UInt32.Parse(param[2]);
            int test_input_count = (int)Math.Ceiling(0.8 * input_count);

            double[][] input = new double[test_input_count][];
            double[][] output = new double[test_input_count][];

            int l = 0;
            for (int i = 1; i <= test_input_count * 2; i++)
            {
                string[] ele = lines[i].Split(null);
                if (i % 2 == 0)
                {
                    output[l] = new double[output_neurons_count];
                    for (int j = 0; j < output_neurons_count; j++)
                    {
                        output[l][j] = double.Parse(ele[j], System.Globalization.CultureInfo.InvariantCulture);
                    }
                    l++;
                }
                else
                {
                    input[l] = new double[input_neurons_count];
                    for (int j = 0; j < input_neurons_count; j++)
                    {
                        input[l][j] = double.Parse(ele[j], System.Globalization.CultureInfo.InvariantCulture);
                    }
                }
            }

            if (czyNormalizowac)
                input = normalize(input);

            const int num_layers = 3;
            const int num_neurons_hidden = 13;
            const float desired_error = 0.001F;
            const uint max_epochs = 2000;
            const uint epochs_between_reports = 100;

            TrainingData data = new TrainingData();
            data.SetTrainData(input, output);
            using (NeuralNet net = new NeuralNet(NetworkType.LAYER, num_layers, input_neurons_count, num_neurons_hidden, output_neurons_count))
            {
                net.ActivationFunctionHidden = ActivationFunction.SIGMOID_SYMMETRIC;
                net.ActivationFunctionOutput = ActivationFunction.SIGMOID_SYMMETRIC;
                net.TrainingAlgorithm = TrainingAlgorithm.TRAIN_INCREMENTAL;
                net.TrainStopFunction = StopFunction.STOPFUNC_BIT;
                net.BitFailLimit = 0.01F;
                net.LearningRate = 0.8f;
                net.LearningMomentum = 0.5f;
                net.InitWeights(data);
                net.TrainOnData(data, max_epochs, epochs_between_reports, desired_error);
                net.Save("language_classify.net");

                //load data for validation
                input = new double[input_count - test_input_count][];
                output = new double[input_count - test_input_count][];
                l = 0;
                for (int i = test_input_count * 2 + 1; i <= input_count * 2; i++)
                {
                    string[] ele = lines[i].Split(null);
                    if (i % 2 == 0)
                    {
                        output[l] = new double[output_neurons_count];
                        for (int j = 0; j < output_neurons_count; j++)
                        {
                            output[l][j] = double.Parse(ele[j], System.Globalization.CultureInfo.InvariantCulture);
                        }
                        l++;
                    }
                    else
                    {
                        input[l] = new double[input_neurons_count];
                        for (int j = 0; j < input_neurons_count; j++)
                        {
                            input[l][j] = double.Parse(ele[j], System.Globalization.CultureInfo.InvariantCulture);
                        }
                    }
                }

                if (czyNormalizowac)
                    input = normalize(input);

                //validation
                DataType range = 0.3;
                DataType[] computeOutput;
                DataType matchCount = 0;
                for (int i = 0; i < input_count - test_input_count; i++)
                {
                    computeOutput = net.Run(input[i]);
                    for (int j = 0; j < output_neurons_count; j++)
                    {
                        if ((computeOutput[j] >= (output[i][j] - range)) &&
                            (computeOutput[j] < (output[i][j] + range)))
                        {
                            matchCount = matchCount + 1;
                        }
                    }
                }
                Console.WriteLine("Poprawnosc " +
                    matchCount * 100 / ((input_count - test_input_count) * output_neurons_count) + "%");
            }
            Console.ReadLine();
        }

        public static double[][] normalize(double[][] data)
        {
            double max = 0.0;
            double[][] result = new double[data.Length][];
            /** szukamy max **/
            for (int i = 0; i < data.GetLength(0); i++)
            {
                for (int j = 0; j < data[0].Length; j++)
                {
                    if (data[i][j] > max)
                        max = data[i][j];
                }
            }

            /** normalizujemy czestotliwosci **/
            for (int i = 0; i < data.GetLength(0); i++)
            {
                result[i] = new double[data[0].Length];
                for (int j = 0; j < data[0].Length; j++)
                {
                    result[i][j] = (double)((double)data[i][j] / (double)max);
                }
            }
            return result;
        }


    };
}