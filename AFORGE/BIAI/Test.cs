﻿using AForge.Neuro;
using AForge.Neuro.Learning;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIAI
{
    class Test
    {
        static string filename = @"frequencies.test";

        public static void testNetwork()
        {
            //load data form file
            string[] lines = File.ReadAllLines(filename);
            string[] param = lines[0].Split(null);
            int neurons_count = Int32.Parse(param[0]);
            int output_neurons_count = Int32.Parse(param[1]);
            double[] input = new double[neurons_count];

            string[] ele = lines[1].Split(null);
            for (int i = 0; i < neurons_count; i++)
            {
                input[i] = double.Parse(ele[i], System.Globalization.CultureInfo.InvariantCulture);
            }

            Console.WriteLine("Wczytuję sieć neuronową");
            // create neural network
            Network network;
            try
            {
                network = Network.Load("Network.bin");
                Double[] netout = network.Compute(input);

                double max = 0.0;
                int index = 0;
                for(int i = 0; i < output_neurons_count; i++)
                {
                    if(netout[i] > max)
                    {
                        max = netout[i];
                        index = i;
                    }
                    Console.WriteLine(GetLanguageByIndex(i) + ": " + netout[i]);
                }
                Console.WriteLine();
                Console.WriteLine("Prawdopodobny jezyk: " + GetLanguageByIndex(index));
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error przy wczytaniu sieci, prawdopodobnie brak pliku \"Net.bin\".");
                Console.ReadLine();
            }

        }

        public static string GetLanguageByIndex(int i)
        {
            string ret = "";
            switch(i)
            {
                case 0:
                    ret = "Polski";
                    break;
                case 1:
                    ret = "Angielski";
                    break;
                case 2:
                    ret = "Niemiecki";
                    break;
                case 3:
                    ret = "Francuski";
                    break;
                case 4:
                    ret = "Hiszpański";
                    break;
                case 5:
                    ret = "Włoski";
                    break;
                case 6:
                    ret = "Rosyjski";
                    break;
                case 7:
                    ret = "Czeski";
                    break;
                case 8:
                    ret = "Turecki";
                    break;
                case 9:
                    ret = "Łaciński";
                    break;
            }
            return ret;
        }
    }
}
