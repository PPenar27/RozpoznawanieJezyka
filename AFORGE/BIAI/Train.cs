﻿using AForge.Neuro;
using AForge.Neuro.Learning;
using System;
using System.IO;

namespace BIAI
{
    class Train
    {
        static string filename = @"frequencies.train";

        public static void trainNetwork()
        {
            Boolean czyNormalizowac = false;
            //load data form file
            string[] lines = File.ReadAllLines(filename);
            string[] param = lines[0].Split(null);
            int input_count = Int32.Parse(param[0]);
            int input_neurons_count = Int32.Parse(param[1]);
            int output_neurons_count = Int32.Parse(param[2]);
            int test_input_count = (int)Math.Ceiling(0.8 * input_count);

            double[][] input = new double[test_input_count][];
            double[][] output = new double[test_input_count][];

            int l = 0;
            for (int i = 1; i <= test_input_count * 2; i++)
            {
                string[] ele = lines[i].Split(null);
                if (i % 2 == 0)
                {
                    output[l] = new double[output_neurons_count];
                    for (int j = 0; j < output_neurons_count; j++)
                    {
                        output[l][j] = double.Parse(ele[j], System.Globalization.CultureInfo.InvariantCulture);
                    }
                    l++;
                }
                else
                {
                    input[l] = new double[input_neurons_count];
                    for (int j = 0; j < input_neurons_count; j++)
                    {
                        input[l][j] = double.Parse(ele[j], System.Globalization.CultureInfo.InvariantCulture);
                    }
                }
            }
            if(czyNormalizowac)
                input = normalize(input);

            Console.WriteLine("Tworzę sieć neuronową...");
            // create neural network
            Network network = new ActivationNetwork(
                new SigmoidFunction(2),
                input_neurons_count,
                16,
                output_neurons_count);
            network.Randomize();

            // create teacher
            BackPropagationLearning teacher = new BackPropagationLearning((ActivationNetwork)network);
            teacher.LearningRate = 0.8;
            teacher.Momentum = 0.5;

            Console.WriteLine("Sieć neuronowa uczy się...");
            DateTime p = DateTime.Now;
            Console.WriteLine("Czas rozpoczęcia: " + p.Hour.ToString() + ":" + p.Minute.ToString() + ":" + p.Second.ToString());
            long epochs = 0;
            long maxEpochs = 2000;
            long printPerEpoch = 100;
            double errorThreshold = 0.001;

            while (true)
            {
                double error = teacher.RunEpoch(input, output);
                error = error / input.Length;
                epochs++;
                if (epochs % printPerEpoch == 0)
                {
                    Console.WriteLine("Epochs: " + epochs + " Current error: " + error);
                }
                if (epochs > maxEpochs)
                {
                    break;
                }
                if (error < errorThreshold)
                    break;
            }
            DateTime k = DateTime.Now;
            Console.WriteLine("Czas zakończenia: " + k.Hour.ToString() + ":" + k.Minute.ToString() + ":" + k.Second.ToString());
            TimeSpan Roznica = k - p;
            Console.WriteLine("Różnica: " + Roznica.Hours.ToString() + ":" + Roznica.Minutes.ToString() + ":" + Roznica.Seconds.ToString());
            network.Save("Network.bin");

            //load data for validation
            input = new double[input_count - test_input_count][];
            output = new double[input_count - test_input_count][];
            l = 0;
            for (int i = test_input_count * 2 + 1; i <= input_count * 2; i++)
            {
                string[] ele = lines[i].Split(null);
                if (i % 2 == 0)
                {
                    output[l] = new double[output_neurons_count];
                    for (int j = 0; j < output_neurons_count; j++)
                    {
                        output[l][j] = double.Parse(ele[j], System.Globalization.CultureInfo.InvariantCulture);
                    }
                    l++;
                }
                else
                {
                    input[l] = new double[input_neurons_count];
                    for (int j = 0; j < input_neurons_count; j++)
                    {
                        input[l][j] = double.Parse(ele[j], System.Globalization.CultureInfo.InvariantCulture);
                    }
                }
            }

            if (czyNormalizowac)
                input = normalize(input);
        
            //validation
            double range = 0.01;
            double[] computeOutput;
            double matchCount = 0;
            for (int i = 0; i < input_count - test_input_count; i++)
            {
                computeOutput = network.Compute(input[i]);
                for (int j = 0; j < output_neurons_count; j++)
                {
                    if ((computeOutput[j] >= (output[i][j] - range)) &&
                        (computeOutput[j] < (output[i][j] + range)))
                    {
                        matchCount = matchCount + 1;
                    }
                }
            }
            Console.WriteLine("Poprawnosc: " +
                matchCount * 100 / ((input_count - test_input_count) * output_neurons_count) + "%");
            Console.ReadLine();
        }

        public static double[][] normalize(double[][] data)
        {
            double max = 0.0;
            double[][] result = new double[data.Length][];
            /** szukamy max **/
            for (int i = 0; i < data.GetLength(0); i++)
            {
                for (int j = 0; j < data[0].Length; j++)
                {
                    if (data[i][j] > max)
                        max = data[i][j];
                }
            }

            /** normalizujemy czestotliwosci **/
            for (int i = 0; i < data.GetLength(0); i++)
            {
                result[i] = new double[data[0].Length];
                for (int j = 0; j < data[0].Length; j++)
                {
                    result[i][j] = (double)((double)data[i][j] / (double)max);
                }
            }
            return result;
        }
    }
}
