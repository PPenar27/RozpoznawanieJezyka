﻿using System;


namespace BIAI
{
    class Program
    {
        static void Main(string[] args)
        {

            while (true)
            {
                Console.WriteLine("Menu:");
                Console.WriteLine("1 - train");
                Console.WriteLine("2 - test");
                Console.WriteLine("3 - clear screen");
                Console.WriteLine("4 - exit");

                string result = Console.ReadLine();
                if (result == "1")
                {
                    Train.trainNetwork();
                }
                else if (result == "2")
                {
                    Test.testNetwork();
                }
                else if (result == "3")
                {
                    Console.Clear();
                }
                else
                {
                    System.Environment.Exit(0);
                }
                Console.WriteLine();
            }
        }
    }
}
